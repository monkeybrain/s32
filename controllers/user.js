const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require('../auth');
const Course = require("../models/Course");
const courseController = require("../controllers/course");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result =>{

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)} 
			} else {
					return false
				}
			}
	})
}


//activity
module.exports.detailsUser = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	})
}



//for enrolling user
//async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user.
module.exports.enroll =async (data) => {

//Using await keyword will allow the enroll method to conplete updating the user be returning a response back
	let isUserUpdated = await User.findById(data.userId).then(user =>{
		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user, error) =>{
			if (error) {
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId: data.userId});

		return course.save().then((course, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

	if(isUserUpdated && isCourseUpdated) {
		return true
	} else {
		return false
	}


}

