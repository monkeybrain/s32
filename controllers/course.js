const Course = require("../models/Course");

module.exports.addCourse = (reqBody) => {

let newCourse = new Course({
	name: reqBody.name,
	description: reqBody.description,
	price: reqBody.price
})	

return newCourse.save().then((course, error) => {
	if(error) {
		return false
	} else {
		return true
	}
	})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		console.log(token)

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return res.send({auth: "failed"})
			} else {
				next()
			}
		})
	} else {
		return res.send({auth: "failed"})
	}
}

module.exports.decode = (token) => {

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if (err) {
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
			}
		})
	} else {
		return ("Not Authorized");
	}
}

//Retrieve all the courses
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
};


//Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

//Retrieve specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

//Update a course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//mini activity

module.exports.archiveCourse = (reqParams,reqBody) => {
	let archivedCourse = {
		isActive: false
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}